Class {
	#name : #GshTerm,
	#superclass : #Object,
	#instVars : [
		'history',
		'shell',
		'buffer'
	],
	#category : #Glash
}

{ #category : #accessing }
GshTerm >> initialize [
    shell := OSSUnixSubprocess new
        command: '/bin/bash';
        createMissingStandardStreams: true;
        run.
]

{ #category : #accessing }
GshTerm >> printStderr [
    ""
    ^ shell stderrStream upToEnd.
]

{ #category : #accessing }
GshTerm >> printStdout [
    ""
    ^ shell stdoutStream upToEnd.
]

{ #category : #accessing }
GshTerm >> read: aString [
    ""
    shell stdinStream
        nextPutAll: aString;
        nextPutAll: String lf;
        flush.
]

{ #category : #accessing }
GshTerm >> readStderr [
    ""
    ^ shell stderrStream upToEnd.
]

{ #category : #accessing }
GshTerm >> readStdout [
    ""
    ^ shell stdoutStream upToEnd.
]

{ #category : #accessing }
GshTerm >> send: aString [
    ""
    shell stdinStream
        nextPutAll: aString;
        nextPutAll: String lf;
        flush.
]
