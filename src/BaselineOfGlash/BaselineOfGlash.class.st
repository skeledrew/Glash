Class {
	#name : #BaselineOfGlash,
	#superclass : #BaselineOf,
	#category : #BaselineOfGlash
}

{ #category : #accessing }
BaselineOfGlash >> baseline: spec [
    <baseline>
    spec for: #common do: [
        spec
            package: 'Glash'.
    ].
]
