Class {
	#name : #LeGshTermSnippetElement,
	#superclass : #LeExternalEvaluatedSnippetElement,
	#category : #'Glash-Snippet'
}

{ #category : #accessing }
LeGshTermSnippetElement >> getTerminal [
    "Find terminal to evaluate the command."
    ^ GshTerm getInstanceWith: self page uidString.
]

{ #category : #accessing }
LeGshTermSnippetElement >> onSnippetViewModelChanged [
	super onSnippetViewModelChanged.
	self updateLanguageLabel.
	self coder
	    shellScriptEvaluator: self getTerminal;
	    pharoBindings: self snippetViewModel snippetBindings.
]

{ #category : #accessing }
LeGshTermSnippetElement >> updateLanguageLabel [
	| label |
	label := 'Shell (Glash)'.
	languageLabel text: (label asRopedText glamorousRegularFont foreground: BrGlamorousColors textMarkupColor).

]
