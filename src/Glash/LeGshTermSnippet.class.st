Class {
	#name : #LeGshTermSnippet,
	#superclass : #LeCodeSnippet,
	#category : #'Glash-Snippet'
}

{ #category : #accessing }
LeGshTermSnippet class >> contextMenuItemSpefication [
	<leSnippetSpecification>
	^ LeContextMenuItemSpecification new
		snippetClass: self;
		title: 'Shell (Glash)'
]

{ #category : #accessing }
LeGshTermSnippet class >> leJsonV3Name [
    ""
	^ 'gshSnippet'
]

{ #category : #accessing }
LeGshTermSnippet class >> leJsonV4Name [
    ""
	^ 'gshSnippet'
]

{ #category : #accessing }
LeGshTermSnippet >> acceptVisitor: aVisitor [
    ""
	^ aVisitor visitGshTermSnippet: self
]

{ #category : #accessing }
LeGshTermSnippet >> asSnippetViewModel [
	<return: #LeSnippetViewModel>
	^ LeGshTermSnippetViewModel new snippetModel: self
]

{ #category : #accessing }
LeGshTermSnippet >> newCoder [
	^ GtGshTermCoderModel new
]
