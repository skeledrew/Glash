"
I am the main entry point of this package. Primary usage is via passing commands to my `#read:` method and presenting the results via `#feedback`.
"
Class {
	#name : #GshTerm,
	#superclass : #Object,
	#instVars : [
		'history',
		'shell',
		'buffer',
		'convertAnsi',
		'errAnsiParser',
		'outAnsiParser',
		'ps1',
		'outAccum',
		'errAccum',
		'feedback',
		'commandHandled',
		'feedbackHandlers',
		'commandHandlers'
	],
	#classVars : [
		'instances'
	],
	#category : #'Glash-Core'
}

{ #category : #accessing }
GshTerm class >> getInstanceWith: anUidString [
    ""
    ^ (instances ifNil: [instances := Dictionary new])
        at: anUidString ifAbsentPut: self new.
]

{ #category : #accessing }
GshTerm >> clearCommand: aString [
    "Clear accumulated output and error text."
    outAccum := '' asRopedText.
    errAccum := '' asRopedText.
]

{ #category : #accessing }
GshTerm >> collectHandlers [
    "Get all gsh*Handlers."
    | pragmas |
    feedbackHandlers := OrderedCollection new.
	commandHandlers := OrderedCollection new.
	pragmas := (PragmaCollector
		filter: [ :prg | {'gshCommandHandler:' . 'gshFeedbackHandler:'} includes: prg selector ]) reset.
	pragmas do: [:prg |
	    (prg method methodClass instanceSide respondsTo: prg method selector)
	        ifFalse: [
	            self error:
	                'Need to set `',
	                prg method methodClass instanceSide name,
	                '>>#',
	                prg method selector asString,
	                '` to class instead of instance'
	        ].
	    prg selector = #'gshFeedbackHandler:' ifTrue: [
	        feedbackHandlers add: {
	            #view->prg arguments first.
	            #method->prg method.
	            #class->prg method methodClass instanceSide.
	            #selector->prg method selector.
	        } asDictionary.
	    ].
	    prg selector = #'gshCommandHandler:' ifTrue: [
	        commandHandlers add: {
	            #cmdRx->prg arguments first.
	            #class->prg method methodClass instanceSide.
	            #selector->prg method selector.
	        } asDictionary.
	    ].
	].
    feedback handlers: feedbackHandlers.
]

{ #category : #accessing }
GshTerm >> convertAnsi [
    ^ convertAnsi.
]

{ #category : #accessing }
GshTerm >> convertAnsi: aBoolean [
    convertAnsi := aBoolean.
]

{ #category : #accessing }
GshTerm >> exitCommand: aString [
    ""
    | result |
	result := self terminate.
    shell := nil.
    ^ result.
]

{ #category : #accessing }
GshTerm >> feedback [
    commandHandled ifTrue: [^ feedback].
    ^ feedback
        at: 'out' put: self printStdout;
        at: 'err' put: self printStderr;
        at: 'code' put: self lastExitCode;
        yourself.
]

{ #category : #accessing }
GshTerm >> feedbackHandlers: aCollection [
    feedbackHandlers := aCollection.
]

{ #category : #accessing }
GshTerm >> gtEnviVarsFor: aView [
    <gtView>
    ^ (shell gtEnvironmentVariablesFor: aView)
        priority: 15.
]

{ #category : #accessing }
GshTerm >> gtPathFor: aView [
    <gtView>
    ^ (shell gtPathFor: aView)
        priority: 16.
]

{ #category : #accessing }
GshTerm >> initDefaultShell [
    shell := OSSUnixSubprocess new
        command: '/bin/bash';
        arguments: {'--noediting' . '-i'};
        createMissingStandardStreams: true;
        run.
    feedback := GshTermFeedback new.
    self clearCommand: nil.
]

{ #category : #accessing }
GshTerm >> initialize [
    self initDefaultShell.
    self collectHandlers.
]

{ #category : #accessing }
GshTerm >> lastExitCode [
    ""
    self read: 'echo $?'.
    0.1 seconds asDelay wait.
    ^ [(shell stdoutStream upToEnd findTokens: String lf) last asRopedText.]
        on: Error do: [:ex | ex].
]

{ #category : #accessing }
GshTerm >> lessCommand: aString [
    ""
    aString asFileReference inspect.
]

{ #category : #accessing }
GshTerm >> printOn: aStream [

	aStream
		"nextPutAll: self class name asString;"
		"nextPut: $(;"
		"nextPutAll: 'Command: ';
		nextPutAll: command asString;"
		nextPutAll: 'Pid: ';
		nextPutAll: shell pid asString;
		nextPutAll: '; Status: ';
		nextPutAll: shell exitStatusInterpreter asString";
		nextPut: $)"
]

{ #category : #accessing }
GshTerm >> printStderr [
    ""
    | text |
    text := shell stderrStream upToEnd.
	text := [GshAnsiParser new parseToRopedText: text]
	    on: Error do: [:ex | self inform: ex asString. text asRopedText].
    ^ errAccum insertText: text at: errAccum size.
]

{ #category : #accessing }
GshTerm >> printStdout [
    ""
    | text |
    text := shell stdoutStream upToEnd.
	text := [GshAnsiParser new parseToRopedText: text]
	    on: Error do: [:ex | self inform: ex asString. text asRopedText].
    ^ outAccum insertText: text at: outAccum size.
]

{ #category : #accessing }
GshTerm >> read: aString [
    ""
    | parts result |
    shell ifNil: [self initDefaultShell].
    commandHandlers do: [:handler |
        (aString matchesRegex: (handler at: #cmdRx))
            ifTrue: [
                result := (handler at: #class)
                    perform: (handler at: #selector)
                    withArguments: {aString . feedback}.
                "result class = GshTermFeedback ifFalse: [
                    self error: 'Command handler must return a GshTermFeedback object'].
                feedback := result."
                result = feedback ifFalse: [
                    self error: 'Command handler must return the provided GshTermFeedback object'].
                commandHandled := true.
                ^ self.
            ]
    ].
	parts := aString findTokens: Character space.
    (self respondsTo: (parts first, 'Command:') asSymbol) ifTrue: [
        feedback at: 'code' put: 0.
        commandHandled := true.
        ^ self
            perform: (parts first, 'Command:') asSymbol
            withArguments: {(' ' join: parts allButFirst)};
            yourself.
    ].
    commandHandled := false..
    shell stdinStream
        nextPutAll: aString;
        nextPutAll: String lf;
        flush.
]

{ #category : #accessing }
GshTerm >> terminate [
    ^ [shell terminate] on: Error do: [:ex | ex].
]
