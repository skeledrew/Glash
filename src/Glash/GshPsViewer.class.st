"
I exemplify the extension of GshTermFeedback. Run by sending the command ""ps aux"" to a terminal, and check the ""Feedback"" view.
"
Class {
	#name : #GshPsViewer,
	#superclass : #Object,
	#instVars : [
		'headerCols',
		'content'
	],
	#category : #'Glash-Examples'
}

{ #category : #accessing }
GshPsViewer class >> gshHandlePsAuxFeedback: aGshTermFeedback [
    ""
    <gshFeedbackHandler: #gtPsAuxFor:>
    | headerRx |
	headerRx := 'USER +PID +%CPU +%MEM +VSZ +RSS +TTY +STAT +START +TIME +COMMAND', String lf, '.*'.
    (aGshTermFeedback out asString matchesRegex: headerRx) ifFalse: [^ GshUnhandled new].
    ^ self new view: aGshTermFeedback out.
]

{ #category : #accessing }
GshPsViewer >> gtPsAuxFor: aView [
    ""
    <gtView>
    ^ aView columnedList
        title: 'Columned List';
        priority: 5;
        items: content;
        column: (String tab join: headerCols) text: [:item | item].
]

{ #category : #accessing }
GshPsViewer >> view: aString [
    ""
    | lines |
	lines := aString findTokens: String lf.
	headerCols := lines first findTokens: ' '.
	content := lines allButFirst.
	^ self.
]
