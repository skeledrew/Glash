"
I exemplify the extension of native command handling in GshTerm. Run by sending the command ""nano path/to/file.txt"" to a terminal, and check the ""Feedback"" view.
"
Class {
	#name : #GshNanoTextViewer,
	#superclass : #Object,
	#category : #'Glash-Examples'
}

{ #category : #accessing }
GshNanoTextViewer class >> gshHandleNanoCommand: aString andReturn: aGshTermFeedback [
    <gshCommandHandler: 'nano .*'>
    ^ aGshTermFeedback native: ((aString findTokens: ' ') last asFileReference).
]

{ #category : #accessing }
GshNanoTextViewer class >> gshHandleNanoFeedback: aGshTermFeedback [
    <gshFeedbackHandler: #gtContentsFor:>
    ^ (([aGshTermFeedback native isFile] on: Error do: [false]))
        ifTrue: [aGshTermFeedback native]
        ifFalse: [GshUnhandled new].
]
