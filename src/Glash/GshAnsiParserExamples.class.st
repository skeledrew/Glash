"
I am supposed to contain examples, but it appears that I am currently empty...
"
Class {
	#name : #GshAnsiParserExamples,
	#superclass : #Object,
	#instVars : [
		'params'
	],
	#category : #'Glash-Examples'
}

{ #category : #accessing }
GshAnsiParserExamples >> sampleTextFromDir [
    ^ '[0m[01;34mbackups[0m
[01;34mbin[0m
[01;32mglamoroustoolkit[0m
GlamorousToolkit.changes
GlamorousToolkit.image
[01;34mgt-extra[0m
[01;34mlib[0m
libBoxer.so
libClipboard.so
libGleam.so
libGlutin.so
libSkia.so
Pharo8.0-32bit-7dc39ed.sources
PharoDebug.log
[01;36mpharo-local[0m'
]
