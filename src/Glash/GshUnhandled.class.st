"
I am an alternative nil return value for `<gshFeedbackHandler: #*>` methods notifying the sender that the feedback wasn't handled.
"
Class {
	#name : #GshUnhandled,
	#superclass : #Object,
	#category : #'Glash-Core'
}

{ #category : #accessing }
GshUnhandled >> doesNotUnderstand: aMessage [
    (aMessage arguments first isKindOf: GtPhlowView)
        ifTrue: [^ aMessage arguments first empty].
]
