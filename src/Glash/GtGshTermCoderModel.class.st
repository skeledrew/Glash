Class {
	#name : #GtGshTermCoderModel,
	#superclass : #GtSourceCoder,
	#instVars : [
		'evaluator',
		'pharoBindings'
	],
	#category : #'Glash-Coder'
}

{ #category : #accessing }
GtGshTermCoderModel >> asCoderUIModel [
    ""
	^ GtSourceCoderViewModel new coder: self
]

{ #category : #accessing }
GtGshTermCoderModel >> bindAndExecute: aSourceString [
    ""
    | runner result |
    runner := self shellScriptEvaluator.
	runner read: aSourceString.
	0.5 second asDelay wait.
	^ runner feedback.
]

{ #category : #accessing }
GtGshTermCoderModel >> computeAst: aSourceString [
    ^ nil.
]

{ #category : #accessing }
GtGshTermCoderModel >> initializeShortcuts: addOns [
	super initializeShortcuts: addOns.

	addOns
		addShortcut: GtSourceCoderDoItShortcut new;
		addShortcut: GtSourceCoderDoItAndInspectShortcut new
]

{ #category : #accessing }
GtGshTermCoderModel >> newCompletionStrategy [
	^ GtCompletionStrategy new
]

{ #category : #accessing }
GtGshTermCoderModel >> pharoBindings [
    ^ pharoBindings.
]

{ #category : #accessing }
GtGshTermCoderModel >> pharoBindings: aDictionary [
    pharoBindings := aDictionary.
]

{ #category : #accessing }
GtGshTermCoderModel >> primitiveDebug: aSourceString inContext: aGtSourceCoderEvaluationContext onFailDo: anEvaluationFailBlock [
	self flag: #TODO.
]

{ #category : #accessing }
GtGshTermCoderModel >> primitiveEvaluate: aSourceString inContext: aGtSourceCoderEvaluationContext onFailDo: anEvaluationFailBlock [
	| result |
	result := self bindAndExecute: aSourceString.
	result associationsDo: [ :binding |
		pharoBindings at: binding key asSymbol put: binding value ].

	^ result
		"at: 'snippetResult'
		ifAbsent: anEvaluationFailBlock"
]

{ #category : #accessing }
GtGshTermCoderModel >> shellScriptEvaluator [
    ^ evaluator.
]

{ #category : #accessing }
GtGshTermCoderModel >> shellScriptEvaluator: aGshTerm [
    ""
    evaluator := aGshTerm.
]
