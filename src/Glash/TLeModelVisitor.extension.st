Extension { #name : #TLeModelVisitor }

{ #category : #'*Glash' }
TLeModelVisitor >> visitGshTermSnippet: aGshTermSnippet [
    ""
	^ self visitTextualSnippet: aGshTermSnippet
]
