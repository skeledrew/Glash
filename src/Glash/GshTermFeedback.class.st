"
I am a container for the results of a command. I provide a composite view called ""Feedback"" for presenting the results in multiple ways.
"
Class {
	#name : #GshTermFeedback,
	#superclass : #Dictionary,
	#instVars : [
		'handlers'
	],
	#category : #'Glash-Core'
}

{ #category : #accessing }
GshTermFeedback class >> new [
    ^ super new
        initialize;
        yourself.
]

{ #category : #accessing }
GshTermFeedback >> code [
    ^ (self at: 'code' ifAbsent: [-1]) asNumber.
]

{ #category : #accessing }
GshTermFeedback >> err [
    ^ self native class = GshUnhandled
        ifTrue: [self at: 'err' ifAbsent: ['' asRopedText]].
]

{ #category : #accessing }
GshTermFeedback >> gtFeedbackFor: aView [
    <gtView>
    | subViews result subView |
	subViews := {
	    (self out gtTextFor: aView) title: 'Out'; priority: 1; yourself.
	    (self err gtTextFor: aView) title: 'Err'; priority: 2; yourself.
	} asOrderedCollection.
	handlers do: [:handler |
	    result := (handler at: #class)
	        perform: (handler at: #selector)
	        withArguments: {self}.
	    subView := result perform: (handler at: #view) withArguments: {aView}.
	    subView priority: subViews size + 1.
	    subViews add: subView.
	].
	^ aView composite
	    title: 'Feedback';
	    priority: 5;
	    views: subViews.
]

{ #category : #accessing }
GshTermFeedback >> gtItemsFor: aView [
    ^ (super gtItemsFor: aView)
        priority: 10.
]

{ #category : #accessing }
GshTermFeedback >> gtKeysFor: aView [
    ^ aView empty.
]

{ #category : #accessing }
GshTermFeedback >> handlers: aCollection [
    handlers := aCollection.
]

{ #category : #accessing }
GshTermFeedback >> initialize [
    super initialize.
]

{ #category : #accessing }
GshTermFeedback >> native [
    "Return a native object or stand-in."
    ^ self at: 'native' ifAbsent: [GshUnhandled new].
]

{ #category : #accessing }
GshTermFeedback >> native: anObject [
    "Allow for a non-text object to be handled."
    ^ self
        at: 'native' put: anObject;
        yourself.
]

{ #category : #accessing }
GshTermFeedback >> out [
    ^ self native class = GshUnhandled
        ifTrue: [self at: 'out' ifAbsent: ['' asRopedText]].
]
