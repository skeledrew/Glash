# Glash
A terminal emulator for [GToolkit](https://gtoolkit.com/).

## Installation
- Install by running in a Playground:
```
Metacello new
    baseline: 'Glash';
    repository: 'gitlab://skeledrew/Glash/src';
    load.
```

## Usage
There are two primary ways to use Glash.
- As a component of another object. An example flow would be:
```
term := GshTerm new.
term read: 'echo "Hello there"'.
term feedback.
term terminate.
```
- As a standalone Lepiter snippet. To use as such, select `Shell (Glash)` in the new snippet dropdown.

In either case, a GshTermFeedback object is returned with the result.

## Extending
Glash can be extended in three ways.
- A command can be handled natively by extending GshTerm with instance methods ending in "Command:". These methods take the command arguments as a string. This method is needed for commands which need direct access to GshTerm instance variables.
- For more flexibility in native command handling, a class method annotated with the pragma `<gshCommandHandler: 'cmd arg'>` can be created. The pragma uses as argument a regex string for matching commands, and the method itself takes a command string and a GshTermFeedback object. The latter should be provided with the results and returned after processing the former. Its use is exemplified in `GshNanoTextViewer`.
- `GshTermFeedback` objects can also be given extra views on the data results it is holding. By creating a class method with the pragma `<gshFeedbackHandler: #gtViewNameFor:`, the feedback object will be passed in during the creation of the "Feedback" view, and it is expected to return an object which supports the view declared by the pragma argument, or an instance of `GshUnhandled` when a result is not handled by the method. Usage is exemplified in `GshPsViewer`.

## Development
- Clone the repo and load via `Gt4Git`.

## Notes
- Glash has been observed to suspend or even terminate the GT process at times. It would be prudent to save state accordingly.

## License
AGPLv3+. See file LICENSE.